const date_picker_element = document.querySelector('.date-picker');
const selected_date_element = document.querySelector('.date-picker .selected-date');
const dates_element = document.querySelector('.date-picker .dates');
const mth_element = document.querySelector('.date-picker .dates .month .mth');
const next_mth_element = document.querySelector('.date-picker .dates .month .next-mth');
const prev_mth_element = document.querySelector('.date-picker .dates .month .prev-mth');
const days_element = document.querySelector('.date-picker .dates .days');
const week_element = document.querySelector('.date-picker .dates .weeks');
let dat = document.querySelector('.dat');
let opDate = document.querySelectorAll('.dat-change');
const months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
const dayOfWeek = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];
let date = new Date();
let day = date.getDate();
let month = date.getMonth();
let year = date.getFullYear();
let week = date.getDay();
for (let opdat of opDate){
	opdat.addEventListener('click', function(){
		document.querySelector('.dates').classList.toggle('active')
		date_picker_element.classList.toggle('showDate');
	})
}


let selectedDate = date;
let selectedDayOne = day;
let selectedMonth = month;
let selectedYear = year;
//это просто выводит массив в блок недели, попробовать сделать функцию, что выводит сначала день недели с которого начинается месяц
dayOfWeek.forEach(function(item, i, arr){
   week_element.innerHTML += '<div class="week">' + item + '</div>';
});
mth_element.textContent = months[month] + ' ' + year;

selected_date_element.textContent = formatDate(date);
selected_date_element.dataset.value = selectedDate;

populateDates();

// EVENT LISTENERS
date_picker_element.addEventListener('click', toggleDatePicker);
next_mth_element.addEventListener('click', goToNextMonth);
prev_mth_element.addEventListener('click', goToPrevMonth);
// FUNCTIONS
function toggleDatePicker (e) {
	if (!checkEventPathForClass(e.path, 'dates')) {
		dates_element.classList.toggle('active');
		
	}
}

function goToNextMonth (e) {
	month++;
	if (month > 11) {
		month = 0;
		year++;
	}
	mth_element.textContent = months[month] + ' ' + year;
	populateDates();
}

function goToPrevMonth (e) {
	month--;
	if (month < 0) {
		month = 11;
		year--;
	}
	mth_element.textContent = months[month] + ' ' + year;
	populateDates();
}

function populateDates (e) {
    days_element.innerHTML = '';
    Date.prototype.daysInMonth = function() {
        return 33 - new Date(year, month, 33).getDate();
    };
    let amount_days = new Date().daysInMonth();
    let firstDayInMonth = new Date(year + "-" + (month+1) + "-01").getDay();
    if (firstDayInMonth == 0) {
        days_element.innerHTML = `<div></div>` + `<div></div>` + `<div></div>` +`<div></div>` + `<div></div>` + `<div></div>`
    }
    if (firstDayInMonth == 2) {
        days_element.innerHTML = `<div></div>`;
    }
    if (firstDayInMonth == 3) {
        days_element.innerHTML = `<div></div>` + `<div></div>`;
    }
    if (firstDayInMonth == 4) {
        days_element.innerHTML = `<div></div>` + `<div></div>` + `<div></div>`;
    }
    if (firstDayInMonth == 5) {
        days_element.innerHTML = `<div></div>` + `<div></div>` + `<div></div>` +  `<div></div>`;
    }
    if (firstDayInMonth == 6) {
        days_element.innerHTML = `<div></div>` + `<div></div>` + `<div></div>` +  `<div></div>` + `<div></div>`;
    }
	for (let i = 0; i < amount_days; i++) {
		const day_element = document.createElement('div');
		day_element.classList.add('day');
        day_element.innerHTML =  i + 1;

		if (selectedDayOne == (i + 1) && selectedYear == year && selectedMonth == month) {
			day_element.classList.add('selected');
		}
		

		day_element.addEventListener('click', function () {
			selectedDate = new Date(year + '-' + (month + 1) + '-' + (i + 1));
			 selectedDayOne = (i + 1);
			selectedMonth = month;
			selectedYear = year;

			selected_date_element.textContent = formatDate(selectedDate);
			selected_date_element.dataset.value = selectedDate;

			populateDates();
		});
		days_element.appendChild(day_element);
	}
}


function checkEventPathForClass (path, selector) {
	for (let i = 0; i < path.length; i++) {
		if (path[i].classList && path[i].classList.contains(selector)) {
			return true;
		}
	}
	
	return false;
}

function formatDate (d) {
	let day = d.getDate();
	if (day < 10) {
		day = '0' + day;
	}

	let month = d.getMonth() + 1;
	if (month < 10) {
		month = '0' + month;
	}

	let year = d.getFullYear();
	return dat.value = day + ' . ' + month + ' . ' + year
	
}