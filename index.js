let op = document.querySelector('.stepper'),
    arrows = document.querySelectorAll('.arr'),
    chmi = document.querySelector('.chmi'),
    place = document.querySelector('.place');
    check = document.querySelector('.checkbox-in');
    check.addEventListener('click', function () {
    document.querySelector('.checkbox-container').classList.toggle('checkbox-container-change');
    })
    place.addEventListener('click', function(){
    document.querySelector('.place__down').classList.toggle('show3');
    document.querySelector('.place__result').classList.toggle('result-change');
  });


for (let arrow of arrows) {
  arrow.addEventListener('click', function() {
    this.textContent = this.textContent === 'keyboard_arrow_up' ? 'keyboard_arrow_down' : 'keyboard_arrow_up';
  } )
}



  
chmi.addEventListener('click',function(event){
	document.querySelector('.downcheck').classList.toggle('show2');
});

//range slider

var inputLeft = document.getElementById("input-left");
var inputRight = document.getElementById("input-right");

var thumbLeft = document.querySelector(".slider > .thumb.left");
var thumbRight = document.querySelector(".slider > .thumb.right");
var range = document.querySelector(".slider > .range");

function setLeftValue() {
	var _this = inputLeft,
		min = parseInt(_this.min),
		max = parseInt(_this.max);

	_this.value = Math.min(parseInt(_this.value), parseInt(inputRight.value) - 1);

	var percent = ((_this.value - min) / (max - min)) * 100;

	thumbLeft.style.left = percent + "%";
	range.style.left = percent + "%";
}
setLeftValue();

function setRightValue() {
	var _this = inputRight,
		min = parseInt(_this.min),
		max = parseInt(_this.max);

	_this.value = Math.max(parseInt(_this.value), parseInt(inputLeft.value) + 1);

	var percent = ((_this.value - min) / (max - min)) * 100;

	thumbRight.style.right = (100 - percent) + "%";
	range.style.right = (100 - percent) + "%";
}
setRightValue();

inputLeft.addEventListener("input", setLeftValue);
inputRight.addEventListener("input", setRightValue);


inputLeft.addEventListener("mousedown", function() {
	thumbLeft.classList.add("active");
});
inputLeft.addEventListener("mouseup", function() {
	thumbLeft.classList.remove("active");
});
inputRight.addEventListener("mousedown", function() {
	thumbRight.classList.add("active");
});
inputRight.addEventListener("mouseup", function() {
	thumbRight.classList.remove("active");
});


